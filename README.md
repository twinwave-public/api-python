# api-python

python library for interacting with the Twinwave API

## Installation

Install by running `pip3 install git+https://gitlab.com/twinwave-public/api-python.git`

# Library Usage

```python
import os
from twinwaveapi.api import TwinwaveMAP as API

api = API(os.environ["TWMP_APIKEY"])
for job in api.poll_done_jobs():
  print(job["ID"])
```

## CLI Usage

To use, set the TWMP_APIKEY environment variable to a valid API key, then run twcli:

```bash
$ export TWMP_APIKEY=...
$ twcli -h
usage: twcli [-h] [-j JOBID] [-cf JOBID] [-u URL] [-f FILE] [-lj] [-le]
             [-e ENGINE] [-p PRIORITY] [--profile PROFILE]
             [--parameter PARAMETER]

optional arguments:
  -h, --help            show this help message and exit
  -j JOBID, --getjob JOBID
                        JobID to query
  -cf JOBID, --consolidated JOBID
                        get consolidated forensics for a jobid
  -u URL, --submiturl URL
                        URL to submit
  -f FILE, --submitfile FILE
                        file to submit
  -lj, --listjobs       list recent jobs
  -le, --listengines    list available engines
  -e ENGINE, --engine ENGINE
                        add the engine to the list of desired engines
  -p PRIORITY, --priority PRIORITY
                        specify a priority (1-255) for the submission. Lower
                        priority values are processed first
  --profile PROFILE     specify an optional submission profile
  --parameter PARAMETER
                        specify 0 or more submission parameters: --parameter
                        key=val --parameter archive_document_password=secret
                        --parameter decode_rewritten_urls=false
```
