#!/usr/bin/env python3
import os
import requests
import json
import time

API_HOST = "https://api.twinwave.io"

# example priority values
PRIORITY_HIGHEST = 1
PRIORITY_HIGH = 5
PRIORITY_NORMAL = 10
PRIORITY_LOW = 15
PRIORITY_LOWER = 20

POLL_SLEEP_SECONDS = 60


class AuthenticationException(Exception):
    pass


class TwinwaveMAP:
    def __init__(self, apikey, host=API_HOST):
        self.host = host
        self.apikey = apikey

    def _get_header(self):
        return {"X-API-KEY": self.apikey}

    def poll_done_jobs(self):
        url = f"{self.host}/v1/jobs/poll"
        consecutive_failures = 0
        next_token = ""
        while consecutive_failures < 5:
            try:
                resp = requests.get(url, params={"token": next_token}, headers=self._get_header())
                resp.raise_for_status()
            except Exception:
                # If we fail too many times on the same token, we want to
                # bail out, so track failures.
                consecutive_failures += 1
                time.sleep(POLL_SLEEP_SECONDS)
                continue

            consecutive_failures = 0
            payload = resp.json()
            next_token = payload["NextToken"]
            jobs = payload["Jobs"]

            if jobs:
                for job in jobs:
                    yield job
            elif next_token:
                # We have a next_token, but no Jobs, backoff the API for a bit
                time.sleep(POLL_SLEEP_SECONDS)
            else:
                return

    def get_recent_jobs(self, numjobs=10):
        url = f"{self.host}/v1/jobs/recent"

        resp = requests.get(url, params={"count": numjobs}, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def get_engines(self):
        url = f"{self.host}/v1/engines"

        resp = requests.get(url, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def get_job(self, jobid):
        url = f"{self.host}/v1/jobs/{jobid}"

        resp = requests.get(url, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def get_forensics(self, jobid, taskid):
        url = f"{self.host}/v1/jobs/{jobid}/tasks/{taskid}/forensics"

        resp = requests.get(url, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def get_consolidated_forensics(self, jobid):
        url = f"{self.host}/v1/jobs/{jobid}/forensics"

        resp = requests.get(url, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def get_temporary_artifact_url(self, path):
        url = f"{self.host}/v1/jobs/artifact/url"

        resp = requests.get(url, headers=self._get_header(), params={"path": path})

        resp.raise_for_status()

        return resp.json()

    def save_artifact(self, path, out_path):
        """This is a convenience method that handles resolving a logical path
        refered to by `path`, and saves it to a file on disk named by
        `out_path`. You do not need to manually get the temorary artifact URL
        when using this helper.
        """
        url = self.get_temporary_artifact_url(path)["URL"]

        resp = requests.get(url)

        resp.raise_for_status()

        with open(out_path, "wb") as f:
            f.write(resp.content)

    def save_pdf_report(self, jobid, out_path):
        url = f"{self.host}/v1/jobs/{jobid}/pdfreport"

        resp = requests.get(url, headers=self._get_header())

        resp.raise_for_status()

        with open(out_path, "wb") as f:
            f.write(resp.content)

    def submit_url(self, scanurl, engine_list=[], priority=PRIORITY_NORMAL, profile="default", parameters=[]):
        url = f"{self.host}/v1/jobs/urls"

        req = {"url": scanurl, "engines": engine_list, "priority": priority, "profile": profile, "parameters": parameters}

        resp = requests.post(url, json=req, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()

    def submit_file(self, fileobj, engine_list=[], priority=PRIORITY_NORMAL, profile="default", parameters=[]):
        url = f"{self.host}/v1/jobs/files"

        resp = requests.post(
            url,
            files={
                "engines": (None, json.dumps(engine_list)),
                "filedata": fileobj,
                "filename": (None, os.path.basename(fileobj.name)),
                "priority": (None, priority),
                "profile": (None, profile),
                "parameters": (None, json.dumps(parameters)),
            },
            headers=self._get_header(),
        )

        resp.raise_for_status()

        return resp.json()

    def delete_job(self, jobid):
        url = f"{self.host}/v1/jobs/{jobid}"

        resp = requests.delete(url, headers=self._get_header())

        resp.raise_for_status()

        return resp.json()
